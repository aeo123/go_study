package main

import (
	"os"
	"github.com/sirupsen/logrus"
	"github.com/gin-gonic/gin"
	. "zlink.ltd/study/src/config"
)

//debug模式输出log到console
//release模式输出log到logs目录下文件
func init(){

	if Config.App.Mode == gin.ReleaseMode{
		//gin.DisableConsoleColor()
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}
}

func main() {



	logrus.Info("Server Starting...")


	logrus.Debug("Server Stopped")

	logrus.Error("Server Killed")


	logrus.WithFields(logrus.Fields{
		"event": "i am event",
		"topic": "i am topic",
		"key": "i am key",
	}).Fatal("Failed to send event")

	os.Exit(0)
}
