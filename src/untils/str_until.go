package untils

import (
	"unsafe"
	"reflect"
)

//字符串转byte
func StringBytes(s string) byte {
	return *(*byte)(unsafe.Pointer(&s))
}

//byte转字符串
func BytesString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

//byte转字符串指针
func BytesStringP(b []byte) *string {
	return (*string)(unsafe.Pointer(&b))
}

//字符串指针
func StringPointer(s string) unsafe.Pointer {
	p := (*reflect.StringHeader)(unsafe.Pointer(&s))
	return unsafe.Pointer(p.Data)
}

//byte指针
func BytesPointer(b []byte) unsafe.Pointer {
	p := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	return unsafe.Pointer(p.Data)
}