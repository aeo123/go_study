package untils

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func LogReqErr(c *gin.Context, err error){
	logrus.WithFields(logrus.Fields{
		"Url": c.Request.RequestURI,
	}).Debug(err)
}
