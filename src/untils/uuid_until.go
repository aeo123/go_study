package untils

import (
	"github.com/satori/go.uuid"
	"strings"
)

func NewUuidV1() (string, error) {
	uid, err := uuid.NewV1()
	if err != nil {
		return "", err
	}
	key := strings.Replace(uid.String(),"-","",-1)
	return key,nil
}
