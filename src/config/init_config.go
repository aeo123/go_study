package config

import (
	"github.com/spf13/viper"
	"fmt"
	"github.com/sirupsen/logrus"
	. "zlink.ltd/study/src/config/param"
)

type ConfigParam struct {
	App    AppParam //应用参数
	Logger LogParam
}

var Config ConfigParam

func init() {
	var b = viper.New()
	b.SetConfigType("yaml")
	b.SetConfigName("bootstrap")   // name of config file (without extension)
	b.AddConfigPath("./resources") // 配置文件路径，多次使用可以查找多个目录
	err := b.ReadInConfig()        // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
		return
	}


	//加载配置
	Config.Logger.ReadConfig(b)
	Config.App.ReadConfig(b)

	logrus.Info("Config Read  Over")
}
