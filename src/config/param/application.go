package param

import (
	"github.com/spf13/viper"
	"github.com/sirupsen/logrus"
)

type server struct {
	Mode string
	Port string
}

type AppParam struct {
	server
}

func (p *AppParam)ReadConfig(v *viper.Viper){

	//default config
	v.SetDefault("application.mode", "debug")
	v.SetDefault("application.server.port","8080")

	//read resources
	p.Mode = v.GetString("application.mode")
	//注意server的port数字前面有个冒号
	p.Port = ":" + v.GetString("application.server.port")

	logrus.WithFields(logrus.Fields{
		"param": p,
	}).Debug("AppParam Read Suc")
}

