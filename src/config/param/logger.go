package param

import (
	_ "github.com/gogap/logrus_mate/writers/rotatelogs"
	"github.com/spf13/viper"
	"github.com/gogap/logrus_mate"
	"github.com/sirupsen/logrus"
	"fmt"
)

type LogParam struct {
}

func (p *LogParam) ReadConfig(v *viper.Viper) {
	var mode = v.GetString("application.mode")

	// 根据应用环境加载相应的log配置
	mate, err := logrus_mate.NewLogrusMate(logrus_mate.ConfigFile("resources/log.conf"))

	//注入logrus的标准logger
	if err = mate.Hijack(logrus.StandardLogger(), mode); err != nil {
		fmt.Println(err)
		return
	}
	//使用
	logrus.Debug("LogParam Read Suc")
}
